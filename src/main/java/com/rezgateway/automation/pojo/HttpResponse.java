package com.rezgateway.automation.pojo;

public class HttpResponse {
	
	private int RESPONSE_CODE    = 100;
	private String RESPONSE      = "N/A" ;
	
	
	public int getRESPONSE_CODE() {
		return RESPONSE_CODE;
	}
	public void setRESPONSE_CODE(int rESPONSE_CODE) {
		RESPONSE_CODE = rESPONSE_CODE;
	}
	public String getRESPONSE() {
		return RESPONSE;
	}
	public void setRESPONSE(String rESPONSE) {
		RESPONSE = rESPONSE;
	}
	
	

}
